
// 3. Add a single room with insertOne

db.rooms.insertOne({
	"name":"single",
	"accomodates":2,
	"price":1000,
	"description":"A simple room with all the basic necessities",
	"rooms_available":10,
	"isAvailable":false
})

// 4. Add multiple rooms with insertMany
db.rooms.insertMany([{	
	"name":"double",
	"accomodates":3,
	"price":2000,
	"description":"A room fit for a small family going on a vacation",
	"rooms_available":5,
	"isAvailable":false},{	
	"name":"double",
	"accomodates":3,
	"price":2000,
	"description":"A room fit for a small family going on a vacation",
	"rooms_available":5,
	"isAvailable":false},{	
	"name":"double",
	"accomodates":3,
	"price":2000,
	"description":"A room fit for a small family going on a vacation",
	"rooms_available":5,
	"isAvailable":false},{	
	"name":"double",
	"accomodates":3,
	"price":2000,
	"description":"A room fit for a small family going on a vacation",
	"rooms_available":5,
	"isAvailable":false},

	{	
	"name":"double",
	"accomodates":3,
	"price":2000,
	"description":"A room fit for a small family going on a vacation",
	"rooms_available":5,
	"isAvailable":true},{	
	"name":"double",
	"accomodates":3,
	"price":2000,
	"description":"A room fit for a small family going on a vacation",
	"rooms_available":5,
	"isAvailable":true},{	
	"name":"double",
	"accomodates":3,
	"price":2000,
	"description":"A room fit for a small family going on a vacation",
	"rooms_available":5,
	"isAvailable":true},{	
	"name":"double",
	"accomodates":3,
	"price":2000,
	"description":"A room fit for a small family going on a vacation",
	"rooms_available":5,
	"isAvailable":true}])
// 4. Add multiple rooms with name queen with insertMany method
db.rooms.insertMany([
	{"name":"queen",
	"accomodates":4,
	"price":4000,
	"description":"A room with a queen sized bed perfect for a simple getaway",
	"rooms_available":15,
	"isAvailable":false},
	{"name":"queen",
	"accomodates":4,
	"price":4000,
	"description":"A room with a queen sized bed perfect for a simple getaway",
	"rooms_available":15,
	"isAvailable":false},
	{"name":"queen",
	"accomodates":4,
	"price":4000,
	"description":"A room with a queen sized bed perfect for a simple getaway",
	"rooms_available":15,
	"isAvailable":false}])
// 5. use find to search a room named double
db.rooms.find({"name":"double"})
// 6. Use updateOne to update queen room and set available room to 0
db.rooms.updateOne({"name":"queen"},{$set:{"rooms_available":0}})
// 7. use deleteMany to delete rooms with 0 availability
db.rooms.deleteMany({"rooms_available":0})